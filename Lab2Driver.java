package cus1156.catlab2;

import java.util.Scanner;


public class Lab2Driver
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		CatManager catMgr = new CatManager();

		int numTabby = catMgr.countColors("tabby");
		int numSpotted = catMgr.countColors("spotted");
		System.out.println("There are " + numTabby + " tabby cats");
		System.out.println("There are " + numSpotted + " spotted cats");

		Cat catObj = new Cat("Mally","black");
        catMgr.add(catObj);
        Cat checkCatObj = catMgr.findThisCat("Mally");
        if(checkCatObj != null){
            System.out.println("Test passed: Cat name: " + checkCatObj.getName() + ", color: "+checkCatObj.getColor());
        }
        else
        {
            System.out.println("Test failed");
        }
		System.out.println("Enter the name of the cat you would like to find");
		String name =  input.next();
		Cat foundCat = catMgr.findThisCat(name);
		if (foundCat == null)
			System.out.println("did not find this cat");
		else
			foundCat.manyMeows(3);
		
		input.close();
	}

}
